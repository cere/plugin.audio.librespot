# plugin.audio.librespot
Unofficial spotify plugin for Kodi, not available in the official Kodi repo.

Uses 'bottle' and 'librespot-python' for playback, and 'spotipy' for the playlist, albums, etc. menus.

Thanks to kokarare1212 for creating ['librespot-python'](https://github.com/kokarare1212/librespot-python/).

This a fork of the [glk1001](https://github.com/glk1001/plugin.audio.spotify) addon, modified to work with librespot-python instead of spotty.

It may take some disabling/re-enabling the addon (or just reopening it a few times) to get it to generate the initial authorization token. Dunno why.
Once credentials are configured, restart Kodi. This is so the background service can setup properly.

## Installation
Download a [zip of this repository](https://gitdab.com/cere/plugin.audio.librespot/archive/master.zip) and install it as an addon. If you're on linux be aware that you will need [inputstream.ffmpegdirect](https://github.com/xbmc/inputstream.ffmpegdirect) which should be in your package manager. Worst case scenario, build it from source.

## Support
None, like my Piped addon this is for personal use. If you wanna update it, fork it (and let me know if you end up doing so!)