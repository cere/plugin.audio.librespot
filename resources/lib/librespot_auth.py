import os, sys
sys.path.insert(1, os.path.join(os.path.dirname(__file__), "deps"))

import time
from typing import Dict

from librespot.core import Session
from utils import log_msg, log_exception, LOGDEBUG

# CLIENT_ID = "2eb96f9b37494be1824999d58028a305"

class LibrespotAuth:
    def __init__(self, session: Session):
        self.__session = session

    def get_token(self) -> Dict[str, str]:
        token_info = None

        try:
            tokenprovider = self.__session.tokens()
            result = tokenprovider.get_token(
                "user-read-playback-state",
                "user-read-currently-playing",
                "user-modify-playback-state",
                "playlist-read-private",
                "playlist-read-collaborative",
                "playlist-modify-public",
                "playlist-modify-private",
                "user-follow-modify",
                "user-follow-read",
                "user-library-read",
                "user-library-modify",
                "user-read-private",
                "user-read-email",
                "user-read-birthdate",
                "user-top-read",
            )

            # Transform token info to spotipy compatible format.
            if result is not None:
                token_info = {
                    "access_token": result.access_token,
                    "expires_in": result.expires_in,
                    "expires_at": int(time.time()) + result.expires_in,
                    "refresh_token": result.access_token,
                }
                
        except Exception as exc:
            log_exception(exc, "Get Spotify token error")
        
        log_msg(f"Token: {token_info}", LOGDEBUG)
        return token_info
