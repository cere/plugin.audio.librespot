import os, sys
sys.path.insert(1, os.path.join(os.path.dirname(__file__), "deps"))

from wsgiref.simple_server import make_server

from bottle import app, request, HTTPResponse, Bottle
import xbmcaddon

from librespot.core import Session
from librespot.audio.decoders import AudioQuality, VorbisOnlyAudioQuality
from librespot.metadata import TrackId

from utils import ADDON_ID, log_msg

class LibrespotServer(Bottle):
    __addon: xbmcaddon.Addon = xbmcaddon.Addon(id=ADDON_ID)

    def __init__(self, session: Session):
        super(LibrespotServer, self).__init__()
        self.session: Session = session
        self.route('/track/<track_id>', callback=self.stream)
        self.is_premium: bool = self.__addon.getSetting("isPremium")

    # TODO: Make Range header work PLEASE I BEG

    def stream(self, track_id):
        try:
            playabletrack_id = TrackId.from_uri(f"spotify:track:{track_id}")

            quality = AudioQuality.NORMAL
            if self.is_premium == True:
                quality = AudioQuality.VERY_HIGH

            stream = self.session.content_feeder().load(
                playabletrack_id, VorbisOnlyAudioQuality(quality), False,
                None)
            start = 0
            end = stream.input_stream.size
            payload = stream.input_stream.stream()
            log_msg(stream.input_stream.size)
            # reqrange = request.get_header("range")
            # if reqrange is not None:
            #     range_search = re.search(
            #         "^bytes=(?P<start>[0-9]+?)-(?P<end>[0-9]+?)$",
            #         reqrange)
            #     if range_search is not None:
            #         start = int(range_search.group("start"))
            #         end = (int(range_search.group("end"))
            #                 if int(range_search.group("end")) <=
            #                 stream.input_stream.size else
            #                 stream.input_stream.size)
            #         payload.skip(start)
            #     else:
            #         payload = stream
            response = HTTPResponse(body=payload)
            response.add_header('Content-Type', 'audio/ogg')
            # response.add_header('Accept-Ranges', 'bytes')
            # response.add_header("Content-Length", str(stream.input_stream.size).encode() if
            #                 stream.input_stream.size == end else "{}-{}/{}"
            #                 .format(start, end,
            #                         stream.input_stream.size).encode())
            return response
        except Exception as e:
            log_msg(e)
