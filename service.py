#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    plugin.audio.librespot
    Spotify player for Kodi
    Main service entry point
"""

if __name__ == "__main__":
    import platform
    import sys
    import os
    from resources.lib.utils import log_msg

    sys.path.insert(1, os.path.join(os.path.dirname(__file__), "resources", "lib", "deps"))

    log_msg(f"Python version: {sys.version}.")
    log_msg(f"Python exe: {sys.executable}.")
    log_msg(f"Platform: {platform.platform()}.")

    from resources.lib.main_service import MainService

    MainService().run()
